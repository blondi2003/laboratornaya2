﻿using System;

namespace nomer4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Название компании
            string name = "Лукойл";
            //Специализация компании
            string name1 = "Нефтегазовая компания";
            //Дата основания
            DateTime date = new DateTime(1991, 11, 25);
            //Глава компании
            string name2 = "Алекперов Вагит Юсуфович";
            //Города,в которых были созданы первые организации
            string name3 = "Лангепас, Урай и Когалым";
            //Количество сотрудников
            int a = 200000;
            //Количество акций (млн)
            int b = 653;
            //Средняя стоимость акций
            int d = 6528;
            //Количество дочерних организаций
            int c = 49;
            Console.WriteLine($"Название компании:{name}");
            Console.WriteLine($"Специализация компании:{name1}");
            Console.WriteLine($"Дата основания:{date}");
            Console.WriteLine($"Глава компании:{name2}");
            Console.WriteLine($"Первые города:{name3}");
            Console.WriteLine($"Количество сотрудников:{a}");
            Console.WriteLine($"Количество акций, млн:{b}");
            Console.WriteLine($"Средняя стоимость акций:{d}");
            Console.WriteLine($"Количество дочерних организаций:{c}");
            Console.ReadKey();
        }
    }
}
