﻿using System;

namespace nomer_2
{
    class MathClass 
    {
        static void Main(string[] args)
        {
            double x, y;
            x = 1;
            y = (1.0 / 19.0) + (Math.Log(Math.Abs(x)) / 7) + (x / 3.0) + Math.Max(x / 4, 2) + Math.Cos(x / 3) + Math.Sin(x / 3) + Math.Pow(x, Math.Pow(x, 2 * x));
            Console.WriteLine(y); 
        }
    }
}
